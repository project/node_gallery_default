<?php
/**
 * @file
 * Views integration for the Default Node Gallery module.
 */

function node_gallery_default_views_data() {
  $data['node_gallery_default']['table']['group'] = t('Node Gallery: Gallery');
  $data['node_gallery_default']['table']['join'] = array(
    'users' => array(
      'left_field' => 'uid',
      'field' => 'uid',
    ),
  );
  $data['node_gallery_default']['rid'] = array(
    'title' => t('Default Gallery'),
    'sort' => array(
      'help' => t('Sort by default gallery flag. Similar to "sticky" node property sorting.'),
      'handler' => 'views_handler_sort',
    ),
    'field' => array(
      'help' => t('Default gallery relationship ID. Empty for nodes that are not default galleries.'),
      'handler' => 'views_handler_field',
    ),
    'filter' => array(
      'help' => t('Filter using default gallery flag.'),
      'handler' => 'views_handler_filter_boolean_operator',
      'type' => 'yes-no',
      'label' => t('Is default gallery'),
      'accept_null' => TRUE,
    ),
  );
  return $data;
}

function node_gallery_default_views_data_alter(&$data) {
  $data['node']['default_gallery_nid'] = array(
    'title' => t("Default Gallery"),
    'help' => t('Relate a node to the list of default galleries.'),
    'group' => t('Node Gallery: Gallery'),
    'relationship' => array(
      'base' => 'node_gallery_default',
      'base field' => 'nid',
      'relationship field' => 'nid',
      'label' => t('default gallery'),
    ),
  );
}
