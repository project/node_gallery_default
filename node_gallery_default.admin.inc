<?php
/**
 * @file
 * Default Node Gallery admin pages code.
 */

function node_gallery_default_settings_form() {
  $access_description = t("<p>Default Node Gallery can work as a simple access 
control module. The main goal is to disallow users to delete their default 
galleries, but <em>update</em> and <em>view</em> access grants can be also 
managed.</p>
<p>Note that <em>view own gallery</em> access is not present here and is always 
granted.</p>
<p><strong>You need basic understanding of Drupal node access system to change 
these settings!</strong></p>");
  
  $delete_description = t(
    'When enabled, Default Node Gallery will allow users to %operation_type 
their <em>non-default</em> galleries. For this setting to work, you need to 
disable corresponding permission for gallery content types.',
    array('%operation_type' => t('delete')) 
  );
  
  $update_description = t(
    'When enabled, Default Node Gallery will allow users to %operation_type 
their <em>non-default</em> galleries. For this setting to work, you need to 
disable corresponding permission for gallery content types.',
    array('%operation_type' => t('edit')) 
  );
  
  $view_description = t("When enabled, Default Node Gallery will allow 
<strong>anyone to view any gallery</strong>. Enable this <strong>only</strong> 
if you need to control access to default galleries, and you don't have any 
other node access module granting view access");
  
  $form['access'] = array(
    '#type' => 'fieldset',
    '#title' => t('Default galleries access control'),
    '#description' => $access_description
  );
  $form['access']['node_gallery_default_manage_delete_grant'] = array(
    '#type' => 'checkbox',
    '#title' => t('Manage %grant_name node access grant', array('%grant_name' => t('delete'))),
    '#description' => $delete_description,
    '#default_value' => variable_get("node_gallery_default_manage_delete_grant", 1),
  );
  $form['access']['advanced_grants'] = array(
    '#type' => 'fieldset',
    '#title' => t('Advanced access control'),
  );
  $form['access']['advanced_grants']['node_gallery_default_manage_update_grant'] = array(
    '#type' => 'checkbox',
    '#title' => t('Manage %grant_name node access grant', array('%grant_name' => t('update'))),
    '#description' => $update_description,
    '#default_value' => variable_get("node_gallery_default_manage_update_grant", 0),
  );
  $form['access']['advanced_grants']['node_gallery_default_manage_view_grant'] = array(
    '#type' => 'checkbox',
    '#title' => t('Manage %grant_name node access grant', array('%grant_name' => t('view'))),
    '#description' => $view_description,
    '#default_value' => variable_get("node_gallery_default_manage_view_grant", 0),
  );
  
  return system_settings_form($form);
}
